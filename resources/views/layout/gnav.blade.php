<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
    <div class="container topnav">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <a href="{!! route('auth.index') !!}" style="position:absolute;"><img src="images/logo.png" alt="Home"
                    title="Home" width="5%" class="img-responsive img-rounded"></a>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <?php $logo = App\Preferences::find(1); ?>
                    <a href="{!! route('auth.index') !!}">
                        <!-- <img src=" data:{!! $logo->company_logo_mime !!};base64,{!! $logo->company_logo_image !!}" alt="Home" title="Home" width="5%" class="img-responsive img-rounded"> -->Accueil
                    </a>
                </li>
                @if (Auth::guest())
                    <li><a href="{!! route('login') !!}">Login</a></li>
                @endif
                @if (Auth::check())
                    <li><a href="{!! route('user.edit', auth()->user()->slug) !!}">Bienvenue {!! auth()->user()->name !!}</a></li>
                    <li>
                        <a href="{!! route('sales.index') !!}">Factures</a>
                    </li>
                    <li>
                        <a href="{!! route('printreport.index') !!}">Imprimer les rapports</a>
                    </li>
                    <li style="margin-top: 8px;">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Gestion des factures<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! route('product.create') !!}">Ajouter un produit</a></li>
                                <li><a href="{!! route('category.create') !!}">Ajouter une catégorie</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! route('customers.index') !!}">Liste des clients</a></li>
                                <li role="separator" class="divider"></li>
                            </ul>
                        </div>
                    </li>
                    @if (auth()->user()->id_group == 1)
                        <li style="margin-top: 8px; margin-left: 5px">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    Paramètres<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{!! route('user.create') !!}">Ajouter un utilisateur</a></li>
                                    <li><a href="{!! route('usergroup.create') !!}">Ajouter un groupe d'utilisateur</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{!! route('banks.index') !!}">Banques</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{!! route('preferences.edit', 1) !!}">Préférences</a></li>
                                    <li role="separator" class="divider"></li>
                                </ul>
                            </div>
                        </li>
                    @endif
                    <li><a href="{!! route('auth.destroy') !!}">Déconnexion</a></li>
                @endif
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

@if (!empty(auth()->user) && auth()->user()->id_group == 1)
    <script type="text/javascript">
        jQuery.noConflict();
        (function($) {
            $(document).ready(function() {
                $('#example').DataTable({
                    // paging: false,
                    "lengthMenu": [
                        [50, -1],
                        [50, "All"]
                    ],
                    // responsive: true
                    order: [
                        [1, 'desc']
                    ],
                });

                @section('jquery')
                @show
            });
        })(jQuery);
    </script>
@else
    <script type="text/javascript">
        jQuery.noConflict();
        (function($) {
            $(document).ready(function() {
                $('#example').DataTable({
                    // paging: false,
                    "lengthMenu": [
                        [50, -1],
                        [50, "All"]
                    ],
                    // responsive: true
                    order: [
                        [0, 'desc']
                    ],
                });

                @section('jquery')
                @show
            });
        })(jQuery);
    </script>
@endif


{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"
    integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous">
</script> --}}

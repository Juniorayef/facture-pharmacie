@extends('layout.master')

@section('content')
    @include('layout.errorform')
    @include('layout.info')

    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Accueil</div>
            <div class="panel-body">
                <h1>BIENVENUE DANS LE SYSTÈME DE GESTION DE FACTURE DE LA PHARMACIE LEBONBERGER IFOPPAG-CEFOPPOL</h1>
                <p>Veuillez vous connecter pour commencer.</p>
            </div>
        </div>
    </div>
@endsection


@section('jquery')
@endsection
